import React from "react";
import styled from "styled-components";


const Paragraph8 = styled.div`
  height: 52px;
  color: #080548;
  font-size: 10px;
  font-weight: 700;
  font-family: Poppins;
  line-height: 40px;
  padding-top: 20px;
  padding-bottom: 20px;
`;

export default function App() {
  return (
    <div className="text-center">
      <Paragraph8>2022 © Parazo Digital Business Solutions Inc.</Paragraph8>
    </div>
  );
}