import { Container, Component } from 'react-bootstrap';
import { Col, Form, Row, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import React, { useState, useEffect, useRef } from 'react';
import emailjs from '@emailjs/browser';
import axios from 'axios';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { Editor } from 'react-draft-wysiwyg';



import styles from "styled-components";
import FormatAlignLeftIcon from '@mui/icons-material/FormatAlignLeft';
import FormatAlignCenterIcon from '@mui/icons-material/FormatAlignCenter';
import FormatAlignRightIcon from '@mui/icons-material/FormatAlignRight';
import FormatAlignJustifyIcon from '@mui/icons-material/FormatAlignJustify';
import FormatBoldIcon from '@mui/icons-material/FormatBold';
import FormatItalicIcon from '@mui/icons-material/FormatItalic';
import FormatUnderlinedIcon from '@mui/icons-material/FormatUnderlined';
import FormatColorFillIcon from '@mui/icons-material/FormatColorFill';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import Divider from '@mui/material/Divider';
import Paper from '@mui/material/Paper';
import ToggleButton from '@mui/material/ToggleButton';


// For Modal
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';

import { styled } from '@mui/material/styles';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';


// const EditorComponent = () => <Editor />

	export default function ContactUs() {

	const [alignment, setAlignment] = useState('left');
  const [formats, setFormats] =  React.useState(() => []);
	
  const [getName, setName] = useState("");
  const [getEmail, setEmail] = useState("");
  const [getConcern, setConcern] = useState("")
  const [getInquiry, setInquiry] = useState("")
  const [getValidName, setValidName] = useState(true);
  const [getValidEmail, setValidEmail] = useState(true);
  const [getValidConcern, setValidConcern] = useState(true)
  const [getValidInquiry, setValidInquiry] = useState(true)
  const [validConvertedText, setValidConvertedText] = useState(true);
  const [isHovering, setIsHovering] = useState(false);
  const [convertedText, setConvertedText] = useState("");
  const [ipAddress, setIpAddress] = useState("")
  const [country, setCountry] = useState("")
  const [city, setCity] = useState("")

  var url = "https://ipgeolocation.abstractapi.com/v1/?api_key=541a7c8e44104a8191dacfaf28ce6d04"

   const form = useRef();

  const handleMouseOver = () => {
    setIsHovering(true);
  };

  const handleMouseOut = () => {
    setIsHovering(false);
  };

  const bgcolor = () => {
  	if(isHovering){
  		return 'rgb(247, 173, 87)'
  	}
  	else {
  		return '#F8931F'
  	}
  }


    const handleFormat = (
    event: React.MouseEvent<HTMLElement>,
    newFormats: string[],
  ) => {
    setFormats(newFormats);
  };

  const handleAlignment = (
    event: React.MouseEvent<HTMLElement>,
    newAlignment: string,
  ) => {
    setAlignment(newAlignment);
  };






  // For Modal
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);



const validate = () => {
	const arr = [getEmail, getConcern, getName, convertedText];
	const error = [];
 arr.forEach(function (task) {
    if(task === ""){
    error.push(task); 
    }
});
	 emptyfields(error)
}

const emptyfields = (error) => {
	for(let i = 0; i < error.length; i++){
		if(0 === i){
			setValidEmail(false)
		} else if (1 === i) {
			setValidConcern(false)
		} else if(2 === i) {
			setValidName(false) 
		} else if (3 === i) {
			setValidConvertedText(false)
		}
	}
}

function submitEmail(e) {
    e.preventDefault()
    if(getEmail !== "" && convertedText !== "" && getConcern !== "" && getName !== "" &&
    	getEmail !== false && convertedText !== false && getConcern !== false && getName !== false){
 			// YOUR_SERVICE_ID, YOUR_TEMPLATE_ID, YOUR_PUBLIC_KEY
    	emailjs.sendForm('service_oase9qr', 'template_e4kl58l', form.current, 'afnpFQF89AtBjo2Eo')
      .then((result) => {

      	emailjs.sendForm('service_oase9qr', 'template_51rzmtd', form.current, 'afnpFQF89AtBjo2Eo')
      .then((data) => { 


      	        fetch(`${window.server}/users/register`, {
						    		method: 'POST',
						    		headers: {
						    			"Content-Type": "application/json"
						    		},
						    		body: JSON.stringify({
										email: getEmail,
										message: convertedText,
										concern: getConcern,
										ipAddress: ipAddress,
										country: country,
										city: city
									})
								})
								.then(res => res.json())
								    .then(data => {

								    	handleClose()	
								    	const Toast = Swal.mixin({
										  toast: true,
										  position: 'bottom-end',
										  showConfirmButton: false,
										  timer: 3000,
										  timerProgressBar: false,
										  didOpen: (toast) => {
										    toast.addEventListener('mouseenter', Swal.stopTimer)
										    toast.addEventListener('mouseleave', Swal.resumeTimer)
										  }
										})
								    e.target.reset()	
								    setOpen(true)
								    setEmail("")
								    setConcern("")
								    setName("")
								    setValidEmail(true)
								    setValidConcern(true)
										setValidName(true) 
										setValidConvertedText(true)

										Toast.fire({
										  icon: 'success',
										  title: 'Email Submitted!'
										})		
						    	})

       }, (error) => {
          console.log(error.text);
      });	

      }, (error) => {
          console.log(error.text);
      });	

    } else {
    	const Toast = Swal.mixin({
				  toast: true,
				  position: 'bottom-end',
				  showConfirmButton: false,
				  timer: 3000,
				  timerProgressBar: false,
				  didOpen: (toast) => {
				    toast.addEventListener('mouseenter', Swal.stopTimer)
				    toast.addEventListener('mouseleave', Swal.resumeTimer)
				  }
				})

				Toast.fire({
				  icon: 'error',
				  title: 'Please check fields'
				})
				validate()
    } 
   }

 useEffect(() => {
 	  var xmlHttp = new XMLHttpRequest();
    xmlHttp.responseType = 'json';
    var data = xmlHttp.response
		xmlHttp.onreadystatechange = function(data) {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200){
      setIpAddress(xmlHttp.response.ip_address);
				setCountry(xmlHttp.response.country);
				setCity(xmlHttp.response.city);
        }

			// console.log(xmlHttp.response)			
    }	
        xmlHttp.open("GET", url, true); // true for asynchronous
    		xmlHttp.send(null);
},[])

	return (

		<div className=" pt-5">
			<div>
		      <Modal
		        open={open}
		        onClose={handleClose}
		        aria-labelledby="modal-modal-title"
		        aria-describedby="modal-modal-description"
		      >
		        <Box sx={style}>
		          <Typography id="modal-modal-title" variant="h6" component="h2">
		            Hello, {getName}!
		          </Typography>
		          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
		            Thank you for your email, we will get back to you as soon as possible.
		          </Typography>
		        </Box>
		      </Modal>
		    </div>


		  <div id="contactUs" className="contact-us">
			<Container className="py-3" >
				<h2 className="text-center"><strong>CONTACT US:</strong></h2>
				<Form className="custom-padding" ref={form} onSubmit={(e) => submitEmail(e)}>
			      <Form.Group as={Row} className="mb-3" >
			        <Form.Label column md="3">
			          Name:
			        </Form.Label>
			        <Col md="9">
			          {
			          	getValidName == false ?
			          	<Form.Control 
					          style={{borderColor:"red",borderWidth: "2px",boxShadow: "none"}}   
					          type="text" 
					          placeholder="Enter Name"
					          value={getName}
					          name="user_name" 
					          onChange={e => {
		                 		 setName(e.target.value)
		                	  }}		           
		                	  />
		               :
		               <Form.Control 
					          style={{borderColor:"#000000",borderWidth: "2px",boxShadow: "none"}}   
					          type="text" 
					          placeholder="Enter Name"
					          value={getName}
					          name="user_name" 
					          onChange={e => {
		                 		 setName(e.target.value)
		                	  }}		           
		                	  />

                }
			        </Col>
			      </Form.Group>

			      <Form.Group as={Row} className="mb-3">
			        <Form.Label column sm="3">
			          Email Address:
			        </Form.Label>
			        <Col sm="9">
			        {
			        	getValidEmail == false ?
			        	<Form.Control 
			          style={{borderColor:"red",borderWidth: "2px",boxShadow: "none"}} 
			          type="email" 
			          placeholder="Enter Email Address"
			          value={getEmail}
			          name="user_email" 
			          onChange={e => {
                 		 setEmail(e.target.value)
                	  }} 
			          />
			          :
			          <Form.Control 
			          style={{borderColor:"#000000",borderWidth: "2px",boxShadow: "none"}} 
			          type="email" 
			          placeholder="Enter Email Address"
			          value={getEmail}
			          name="user_email" 
			          onChange={e => {
                 		 setEmail(e.target.value)
                	  }} 
			          />
			        }
			        </Col>
			      </Form.Group>

			      <Form.Group as={Row} className="mb-3">
			        <Form.Label column sm="3">
			          Concern:
			        </Form.Label>
			        <Col sm="9">
			         { 
			         	getValidConcern == false ?
			         	<Form.Select 
			          style={{borderColor:"red",borderWidth: "2px",boxShadow: "none"}}  
			          aria-label="Default select example"
			          onChange={e => {
                 		 setConcern(e.target.value)
                	  }}
                placeholder="Option"
			          >
				      <option>Select</option>
				      <option value="Web Development">Web Development</option>
				      <option value="Video and Photo Production">Video and Photo Production</option>
				      <option value="Social Media Management">Social Media Management</option>
				      <option value="Community Management<">Community Management</option>
				    </Form.Select>
				    :
				    <Form.Select 
			          style={{borderColor:"#000000",borderWidth: "2px",boxShadow: "none"}}  
			          aria-label="Default select example"
			          name="concern"
			          onChange={e => {
                 		 setConcern(e.target.value)
                	  }}
                placeholder="Option"
			          >
			        <option>Select</option> 
				      <option value="Web Development">Web Development</option>
				      <option value="Video and Photo Production">Video and Photo Production</option>
				      <option value="Social Media Management">Social Media Management</option>
				      <option value="Community Management<">Community Management</option>
				    </Form.Select>

				  }
			        </Col>
			      </Form.Group>
			      <Form.Group  as={Row} className="mb-3">
			        <Form.Label column sm="3">
			          Inquiry:
			        </Form.Label>
			        <Col sm="9">
			         {/*<Editor					  
			         	  wrapperClassName="wrapper-class"
								  editorClassName="editor-class"
								  toolbarClassName="toolbar-class"
								  placeholder="How Can We Help You?"
								  name="message"
								  onChange={e => {
								  		console.log(e.target.value)
	                 		 setEmail(e.target.value)
	                	  }} 
								    toolbar={{
								    options: ['inline', 'textAlign'],
								    inline: { options: ['bold', 'italic', 'underline']},
								    bold: { icon: 'https://file.rendit.io/n/7uJ9glkS8qQ0Zd98M0QD.svg', className: "button-class" },
								    italic: { icon: 'https://file.rendit.io/n/ZmGOl3P2SSKLUu8UxhT7.svg', className: "button-class" },
								    underline: { icon: 'https://file.rendit.io/n/DDnnUu08a5IakEeBKWkW.svg', className: "button-class" },
								    textAlign: { options: ['left', 'center', 'right'] },
								    left: { icon: 'https://file.rendit.io/n/k1vtnml1cfhPnb4PFiG4.svg', className: "button-class" },
								    center: { icon: 'https://file.rendit.io/n/uVPXilXCYy9epgYfVGSN.svg', className: "button-class" },
								    right: { icon: 'https://file.rendit.io/n/zsnEhjgfYpDtN4G3dgMb.svg', className: "button-class" }
								  	}}
								/>	*/}
								 <TextEditorRoot>
									      {/*<ReactQuill
									        theme='snow'
									        value={convertedText}
									        onChange={setConvertedText}
									        style={{minWidth: '480px', maxWidth: '480px', minHeight: '200px'}}
									        placeholder="How can we help you?"
									      />*/}

							    <Form.Control
				          style={{borderColor:"#FFFFFF",borderWidth: "2px", boxShadow: "none",textAlign: alignment}}
				          as="textarea" rows={3}
				          placeholder="How can we help you?"
				          name="message"
				          onChange={e => {
	                 		 setConvertedText(e.target.value)
	                	  }} 
				          />

						      <Toolbar id="toolbar">
						        <FlexRow>
						        <StyledToggleButtonGroup
				          size="small"
				          value={formats}
				          onChange={handleFormat}
				          aria-label="text formatting"
				        >
				          <ToggleButton value="bold" aria-label="bold">
				            <img 
				            onclick={"document.execCommand( 'bold',false,null);"}
          					src={('https://file.rendit.io/n/7uJ9glkS8qQ0Zd98M0QD.svg')}
       							 />
				          </ToggleButton>
				          <ToggleButton value="italic" aria-label="italic">
				            <img 
				            onclick={"document.execCommand( 'bold',false,null);"}
          					src={(`https://file.rendit.io/n/ZmGOl3P2SSKLUu8UxhT7.svg`)}
       							 />
				          </ToggleButton>
				          <ToggleButton value="underlined" aria-label="underlined">
				            <img 
				            onclick={"document.execCommand( 'bold',false,null);"}
          					src={(`https://file.rendit.io/n/DDnnUu08a5IakEeBKWkW.svg`)}
       							 />
				          </ToggleButton>
				        </StyledToggleButtonGroup>
						       
						        </FlexRow>
						        <FlexRow2>
						         <StyledToggleButtonGroup
				          size="small"
				          value={alignment}
				          exclusive
				          onChange={handleAlignment}
				          aria-label="text alignment"
				        >
				          <ToggleButton value="left" aria-label="left aligned">
				            <img 
				            onclick={"document.execCommand( 'bold',false,null);"}
          					src={(`https://file.rendit.io/n/k1vtnml1cfhPnb4PFiG4.svg`)}
       							 />
				          </ToggleButton>
				          <ToggleButton value="center" aria-label="centered">
				            <img 
				            onclick={"document.execCommand( 'bold',false,null);"}
          					src={(`https://file.rendit.io/n/uVPXilXCYy9epgYfVGSN.svg`)}
       							 />
				          </ToggleButton>
				          <ToggleButton value="right" aria-label="right aligned">
				            <img 
				            onclick={"document.execCommand( 'bold',false,null);"}
          					src={(`https://file.rendit.io/n/zsnEhjgfYpDtN4G3dgMb.svg`)}
       							 />
				          </ToggleButton>
				        </StyledToggleButtonGroup>
						      </FlexRow2>
						      </Toolbar>
						    </TextEditorRoot>
			        </Col>
			      </Form.Group>
			      <div className="text-center" >
			      <Button type="submit" value="Send" style={{border: "none",boxShadow: "none", fontFamily: "Inter", whiteSpace: "nowrap"}}
			        className="SubmitButton m-2">SUBMIT</Button>
			      </div>
			    </Form>
			</Container>
			</div>
		</div>
		)
}

const FlexRow = styles.div`
  gap: 8px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
`;
const Bold = styles.img`
  width: 24px;
  height: 24px;
`;
const TextEditorRoot = styles.div`
  gap: 8px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  border-width: 2px;
  border-radius: 8px;
  border-style: solid;
  border-color: #000000;
  background-color: #ffffff;
  overflow: hidden;
`;

const Toolbar = styles.div`
  padding: 0.5rem;
  gap: 10px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
`;
const FlexRow2 = styles.div`
  gap: 8px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
`;
const Image1 = styles.div`
  width: 24px;
  height: 24px;
  position: relative;
`;
const FlexColumn = styles.div`
  height: 15.5px;
  left: 2.25px;
  top: 2.25px;
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding: 0.5px 3.5px 0.5px 6.5px;
  border-width: 1.5px;
  border-radius: 0.5px;
  border-style: solid;
  border-color: #000000;
`;
const Oval = styles.img`
  width: 6.5px;
  height: 6.5px;
`;
const Path = styles.img`
  width: 16.5px;
  height: 12.29px;
  left: 2.25px;
  top: 9.46px;
  position: absolute;
`;
const Path1 = styles.img`
  width: 8.56px;
  height: 8.56px;
  left: 12.97px;
  top: 8.47px;
  position: absolute;
`;

//For Modal
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const StyledToggleButtonGroup = styled(ToggleButtonGroup)(({ theme }) => ({
  '& .MuiToggleButtonGroup-grouped': {
    margin: theme.spacing(0),
    border: 0,
    '&.Mui-disabled': {
      border: 0,
    },
    '&:not(:first-of-type)': {
      borderRadius: theme.shape.borderRadius,
    },
    '&:first-of-type': {
      borderRadius: theme.shape.borderRadius,
    },
  },
}));
